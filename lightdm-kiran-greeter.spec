Name:		lightdm-kiran-greeter
Version:	2.1.2
Release:  	3
Summary:	User login interface
Summary(zh_CN): 用户登录界面

License: 	MulanPSL-2.0
#URL:
Source0:	%{name}-%{version}.tar.gz

#%define BIOMETRICS_AUTH false
%define SHOW_VIRTUAL_KEYBOARD false

BuildRequires:  cmake
BuildRequires:  gcc-c++
BuildRequires:  lightdm-qt5
BuildRequires:  lightdm-qt5-devel
BuildRequires:  qt5-qtbase
BuildRequires:  qt5-qtbase-devel
BuildRequires:  libX11
BuildRequires:  libX11-devel
BuildRequires:  libXtst
BuildRequires:  libXtst-devel
BuildRequires:  libXrandr
BuildRequires:  libXrandr-devel
BuildRequires:  libXcursor
BuildRequires:  libXcursor-devel
BuildRequires:  libXfixes
BuildRequires:  libXfixes-devel
BuildRequires:  glib2
BuildRequires:  glib2-devel
BuildRequires:  gsettings-qt
BuildRequires:  gsettings-qt-devel
BuildRequires:  qt5-qtx11extras
BuildRequires:  qt5-qtx11extras-devel
BuildRequires:  kiran-widgets-qt5
BuildRequires:  kiran-widgets-qt5-devel
BuildRequires:  qt5-linguist
BuildRequires:  kiran-log-qt5-devel
BuildRequires:  kiran-cc-daemon-devel >= 2.0.3-1
BuildRequires:  kiran-control-panel-devel

%if %{BIOMETRICS_AUTH} == true
BuildRequires:  kiran-biometrics-devel
%endif

Requires:  lightdm
Requires:  lightdm-qt5
Requires:  qt5-qtbase
Requires:  libX11
Requires:  libXtst
Requires:  libXrandr
Requires:  libXcursor
Requires:  libXfixes
Requires:  glib2
Requires:  gsettings-qt
Requires:  qt5-qtx11extras
Requires:  kiran-widgets-qt5
#Requires:  kiran-biometrics
Requires:  kiran-log-qt5
Requires:  kiran-system-daemon >= 2.0.3-1
Requires:  kiran-cpanel-launcher

%if %{BIOMETRICS_AUTH} == true
Requires:       kiran-biometrics
%endif

%if %{SHOW_VIRTUAL_KEYBOARD} == true
Requires: onboard
%endif

Provides:  lightdm-greeter

%description
User login interface

%prep
%autosetup -p1

%build
%{__mkdir} -p %{buildroot}

%cmake \
%if %{BIOMETRICS_AUTH} == true
-DHAVE_BIOMETRICS_AUTH=on \
%endif
%if %{SHOW_VIRTUAL_KEYBOARD} == true
-DSHOW_VIRTUAL_KEYBOARD=on \
%endif

make %{?_smp_mflags}


%install
%make_install

%post
gtk-update-icon-cache -f /usr/share/icons/hicolor/

%posttrans
%define link_source %{_datadir}/kiran-control-panel/plugins/desktop/kiran-cpanel-greeter.desktop
%define link_target %{_datadir}/applications/kiran-cpanel-greeter.desktop
if [ ! -e %{link_target} ] ; then 
	ln -sf %{link_source} %{link_target}
	echo "link %{link_source} -> %{link_target}"
fi

%files
%doc
%{_datadir}/kiran-control-panel/plugins/libs/libkiran-cpanel-greeter.so*
%{_datadir}/kiran-control-panel/plugins/desktop/*
%{_datadir}/icons/hicolor/*
%{_datadir}/applications/*.desktop

%{_sbindir}/%{name}
%{_datadir}/%{name}/translations/*
%{_datadir}/%{name}/greeter.ini
%{_datadir}/%{name}/zlog.conf
%{_datadir}/xgreeters/%{name}.desktop
%{_datadir}/lightdm/lightdm.conf.d/*-%{name}.conf

%clean
rm -rf %{buildroot}

%changelog
* Wed Aug 10 2022 luoqing <luoqing@kylinsec.com.cn> - 2.1.2-3
- KYOS-F: Modify license and add yaml file.

* Mon Oct 18 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 2.1.2-1.kb2
- KYBD: rebuild for KiranUI-2.1-OE

* Mon Oct 18 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 2.1.2-1.kb1
- KYOS-B: fix scale mode combobox not diable
- KYOS-B: when all power options are disabled,this power button is hidden

* Sat Oct 09 2021 xiewenhao <xiewenhao@kylinos.com.cn> - 2.1.1-1.kb2
- KYBD: rebuild for KiranUI-2.1-OE

* Tue Aug 03 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 2.1.1-1.kb1
- KYOS-B: fix private zlog.conf
- KYOS-B: fix auth message queue delay

* Wed Jul 21 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 2.1.0-8.kb1
- KYOS-B: install desktop link (#38890)

* Tue Jul 13 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 2.1.0-7.kb1
- KYOS-F: add login interface to hide user and power options(#40972)

* Thu Jul 08 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 2.1.0-6.kb1
- KYOS-F: modify "enable manual login" to "enable manual input user login"
- KYOS-F: unified authentication failure prompt to "Failed to authenticate"

* Wed Jun 30 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 2.1.0-5.kb1
- KYOS-B: don't exit if translation file not found (#40693)

* Tue Jun 29 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 2.1.0-4.kb1
- KYOS-B: unified user name for interface description (#40542)

* Thu Jun 17 2021 longcheng <longcheng@kylinos.com.cn> - 2.1.0-3.kb2
- KYOS-F: change SHOW_VIRTUAL_KEYBOARD to flase

* Wed Jun 16 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 2.1.0-3.kb1
- KYOS-B: Account disabled authentication error message optimized (#40170)

* Tue Jun 15 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 2.1.0-2.kb1
- KYOS-B: fix load default background error

* Wed Jun 09 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 2.1.0-1.kb1
- KYOS-F: using private zlog.conf

* Wed Jun 09 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 1.1.0-8.kb1
- KYBD: add icons

* Mon Jun 07 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 1.1.0-7.kb1
- KYOS-F: using klog-qt5 for log output
- KYOS-F: build for KiranControlPanel plugin

* Tue Jun 01 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 1.1.0-6.kb1
- KYOS-F: virtual keyboard size is calculated form the resolution of the main screen
- KYOS-B: fix kiran-greeter-settings desktop name error

* Tue May 25 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 1.1.0-5.kb1
- KYOS-F: add condition to compile whether to display virtual keyboard 

* Fri May  14 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 1.1.0-4.kb1
- KYOS-B: fix the problem that the default background picture is not visible in the picture selection control
- KYOS-B: fix kiran-greeter-settings translation
- KYOS-F: discard lightdm-kiran-greeter.conf
- KYOS-F: adapting new greeter settings back end

* Sat May  08 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 1.1.0-3.kb1
- KYOS-F: update translation 

* Fri May  07 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 1.1.0-2.kb1
- KYOS-F: using new image select widget

* Thu Apr  29 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 1.1.0-1.kb1
- KYOS-F: using kiranstyle instead of style sheet
- KYOS-F: using DBUS backend,modify interface style
- KYOS-F: fix the possible error of mouse event click position in login interface
- KYOS-F: using zlog for log output
 
* Sat Mar  27 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 1.0.2-1.kb1
- KYOS-F: Add face fingerprint authentication function (#35698)

* Tue Mar  23 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 1.0.1-5.kb2
- KYBD: rebuild for kiranwidgets-qt5-2

* Fri Jan  29 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 1.0.1-5.kb1
- KYOS-B: fix the problem of blocking communication with X when SIGTERM exit release is received (#34254)

* Fri Jan  22 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 1.0.1-4.kb1
- KYOS-F: when authentication failed without input,show re-authentication button
- KYOS-B: fix the possible error of mouse event click position in login interface

* Sat Nov  28 2020 liuxinhao <liuxinhao@kylinos.com.cn> - 1.0.1-3.kb3
- KYBD: rebuild for kiranwidgets-qt5-2

* Wed Oct  28 2020 liuxinhao <liuxinhao@kylinos.com.cn> - 1.0.1-3.kb2
- KYOS-R: rebuild

* Wed Oct  28 2020 liuxinhao <liuxinhao@kylinos.com.cn> - 1.0.1-3.kb1
- KYOS-B: update translation
 
* Tue Oct  27 2020 liuxinhao <liuxinhao@kylinos.com.cn> - 1.0.1-2.kb1
- KYOS-B: Manual login input error account prompt modification (#31164 #31159) 

* Mon Oct  26 2020 longcheng <longcheng@kylinos.com.cn> - 1.0.1-1.kb3
- KYOS-F: Add Obsoletes: slick-greeter-cinnamon, lightdm-settings, and slick-greeter (#30772)
- KYOS-F: Add Provides: lightdm-greeter (#30772)

* Fri Oct  23 2020 longcheng <longcheng@kylinos.com.cn> - 1.0.1-1.kb2
- KYOS-R: rebuild for KY3.3-9-dev (#31162)

* Wed Oct  21 2020 liuxinhao <liuxinhao@kylinos.com.cn> - 1.0.1-1.kb1
- KYOS-B: disable lineedit context menu
- KYOS-B: disable virtual keyboard buttons

* Tue Sep  29 2020 liuxinhao <liuxinhao@kylinos.com.cn> - 1.0.0+beta13  
- KYOS-F: use custom title bar
- KYOS-F: dealing with users from scratch and from being to nothing
- KYOS-B: fix memory leak caused by incorrect use of libxrandr interface
- KYOS-F: kiran-greeter-settings do not handle scaling

* Thu Sep  17 2020 liuxinhao <liuxinhao@kylinos.com.cn> - 1.0.0~beta12
- KYOS-B: log files are truncated every time the log module is initialized

* Mon Sep  14 2020 liuxinhao <liuxinhao@kylinos.com.cn> - 1.0.0~beta11
- KYOS-B: modify the GreeterScreenManager destructor (destruct after the end of the event loop) to release the child object by using deletelater to cause the sub object to be unreleased, modify the deadlock caused by calling waitForFinished into free during soft interrupt
 
* Wed Sep  09 2020 liuxinhao <liuxinhao@kylinos.com.cn> - 1.0.0~beta10
- KYOS-B: fix the problem of synchronization between threads,resulting in incomplete processing of messages in PAM message queue

* Thu Sep  03 2020 liuxinhao <liuxinhao@kylinos.com.cn> - 1.0.0~beta9
- KYOS-B: remove the log output of reply lightdm

* Thu Aug  27 2020 liuxinhao <liuxinhao@kylinos.com.cn> - 1.0.0~beta8
- KYOS-B: when the authentication starts, the input box is disabled and the prompt message is received to enable the input box

* Wed Aug  26 2020 liuxinhao <liuxinhao@kylinos.com.cn> - 1.0.0~beta7
- KYOS-B: power menu ordering standardization
- KYOS-F: setting the background image will copy the background image to the fixed directory and set the permissions

* Tue Aug  18 2020 liuxinhao <liuxinhao@kylinos.com.cn> - 1.0.0~beta6
- KYOS-F: use QT interface to produce fuzzy background
- KYOS-B: the login interface sets the initial NumLock on and capslock off status

* Wed Aug  12 2020 liuxinhao <liuxinhao@kylinos.com.cn> - 1.0.0~beta5
- KYOS-F: auto login user changed to drop-down selection
- KYOS-B: message queue is locked and synchronized with semaphore operation
- KYOS-B: fix onboard crash on exit
- KYOS-B: cursor scaling does not use the system default size

* Thu Aug  06 2020 wangyongchun <wangyongchun@kylinos.com.cn> - 1.0.0~beta4
- KYOS-F: fix hide kiran greeter settings from start menu.(#28554)

* Wed Aug  05 2020 liuxinhao <liuxinhao@kylinos.com.cn> 1.0.0~beta3
- KYOS-T: fix translation errors

* Tue Aug  04 2020 liuxinhao <liuxinhao@kylinos.com.cn> 1.0.0~beta2
- KYOS-F: set the cursor size of login interface according to the zoom setting of login interface

* Mon Jul  27 2020 liuxinhao <liuxinhao@kylinos.com.cn> 1.0.0~beta1
- KYOS-F: extract style sheet to file
- KYOS-F: update README.md
- KYOS-B: dont call gettext to translate messages from lightdm
- KYOS-F: lightdm greeter message queuing display to avoid message coverage
- KYOS-F: when there are no other users, you can directly enter the user name and log in manually

* Thu Jul  09 2020 liuxinhao <liuxinhao@kylinos.com.cn> 0.0.1~alpha11
- KYOS-F: add font shadow
- KYOS-B: let Kiran greeter settings appear in the control center
- KYOS-F: press ESC to clear the password entry

* Tue Jul  07 2020 liuxinhao <liuxinhao@kylinos.com.cn> 0.0.1~alpha10
- KYOS-B: the font of setting interface changes with the system
- KYOS-B: fix the problem that the file selection box opened in the login settings interface is different from the system file selection box

* Fri Jul  03 2020 liuxinhao <liuxinhao@kylinos.com.cn> 0.0.1~alpha9
- KYOS-B: fixed a situation where user entry focus directly changes the current line in some environments,
- KYOS-B: Prohibit users from pressing the mouse and dragging to switch users

* Thu Jul 02 2020 liuxinhao <liuxinhao@kylinos.com.cn> 0.0.1~alpha8
- KYOS-B: fix up and down switch user authentication 

* Thu Jul 02 2020 liuxinhao <liuxinhao@kylinos.com.cn> 0.0.1~alpha7
- KYOS-B: add button tooltips,fix picture stretch size is wrong
- KYOS-F: add login interface settings
- KYOS-B: change default background path,background image missing show black,readme add new dependency
- KYOS-F: displays the user logged in status
- KYOS-F: add keyboard navigation
- KYOS-F: add autologin ui
- KYOS-F: listwidget disable Ctrl deselect,add translation
- KYOS-B: add a description that takes effect after restart,modify the appropriate font size

* Wed Jun 17 2020 liuxinhao <liuxinhao@kylinos.com.cn> 0.0.1~alpha6
- KYOS-B: fix authentication failed,in the case of recertification, the user account picture is restored
- KYOS-B: fix in the user avatar widget, when setting the picture, the pull-up parameter is different from resize, resulting in the picture stretching is not uniform.
- KYOS-B: fix the redraw method was not called when setting the default account picture.

* Fri Jun 12 2020 liuxinhao <liuxinhao@kylinos.com.cn> 0.0.1~alpha5
- KYOS-B: do not pack /lightdm-kiran-greeter

* Mon Jun 01 2020 liuxinhao <linuxinhao@kylinos.com.cn> 0.0.1~alpha4
- KYOS-B: set root window cursor

* Thu May 28 2020 liuxinhao <linuxinhao@kylinos.com.cn> 0.0.1~alpha3
- KYOS-F: increase the adaptation to high-dpi displays and increase the stretch configuration items

* Wed May 27 2020 liuxinhao <linuxinhao@kylinos.com.cn> 0.0.1~alpha2
- user delete add update interface
- fix automatic hiding of power menu and session menu
- fix the problem that the password box has no content and cannot be verified
- modify the white loading animation, modify the user to display up to five,the user list scroll bar is often displayed
- modify the menu to add the popup property window manager to close or the menu is not supported when the graphics card does not support the problem
- modify the not list button description, add underline

* Thu May 21 2020 liuxinhao <liuxinhao@kylinos.com.cn> 0.0.1~alpha1
- Initial build
